def num1(num)
  if num < 0
    puts "You can't enter a negative number!"
  elsif num <= 50
      puts "#{num} is between 0 and 50"
  elsif num <= 100
    puts "#{num} is between 51 and 100"
  else 
      puts "#{num} is greater than 100"
  end
end

def num2(num)
  case
    when num < 0
    puts "You can't enter a negative number!"
    when num <= 50
      puts "#{num} is between 0 and 50"
    when num <= 100
      puts "#{num} is between 51 and 100"
    else 
      puts "#{num} is greater than 100"
  end
end 

def num3(num)
  case num
  when 0..50
    puts "#{num} is between 0 and 50"
  when 51..100
    puts "#{num} is between 51 and 100"
  else
    if num < 0
      puts "You can't enter a negative number!"
    else
      puts "#{num} is greater than 100"
    end
  end
end

num = ""
while num == ""
  puts "Please enter a number between 0 and 100."
  num = gets.chomp
end
num = num.to_i

num1(num)
num2(num)
num3(num)