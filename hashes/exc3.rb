hash = {hot: "streak", fish: "burger"}

puts hash.keys
puts hash.values
hash.each { |k,v| puts "#{k}: #{v}"}

hash.each_key { |key| puts key }
hash.each_value { |value| puts value }
