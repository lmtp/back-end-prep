hash1 = {bird: "Parrot", dog: "Boxer"}
hash2 = {monkey: "Gorilla", shark: "Orca"}

p hash1.merge hash2
p hash1
p hash2

p hash1.merge! hash2
p hash1
p hash2